<?php

/*
 * Your task in order to complete this Kata is to write a function which formats a duration, given as a number of seconds, in a human-friendly way.

The function must accept a non-negative integer. If it is zero, it just returns "now". Otherwise, the duration is expressed as a combination of years, days, hours, minutes and seconds.

It is much easier to understand with an example:

format_duration(62)    # returns "1 minute and 2 seconds"
format_duration(3662)  # returns "1 hour, 1 minute and 2 seconds"
 */

function addSpaceIfNotEmpty(string $string):string {
    if ($string!="") {
        return ", ";
    } else {
        return "";
    }
}

function addDurationVerbiage(string &$format, int $duration, string $verbiage) {
    if ($duration > 0) {
        $format .= addSpaceIfNotEmpty($format) . "$duration $verbiage" . ($duration > 1 ? "s" : "");
    }
}

function format_duration(int $seconds):string {
    if ($seconds == 0) {
        return 'now';
    }

    $format = '';
    $years = floor($seconds/(60*60*24*365));

    addDurationVerbiage($format, $years, "year");

    if ($years>0) {
        $seconds -= $years * 60 * 60 * 24 * 365;
    }

    $days = floor($seconds / (60*60*24));

    addDurationVerbiage($format, $days, "day");

    if ($days > 0){
        $seconds -= $days * 60 * 60 * 24;
    }

    $hours = floor($seconds / 3600);

    addDurationVerbiage($format, $hours, "hour");

    if ($hours > 0) {
        $seconds -= $hours * 60 * 60;
    }

    $minutes = floor($seconds / 60);

    addDurationVerbiage($format, $minutes, "minute");

    if ( $minutes > 0 ) {
        $seconds -= $minutes * 60;
    }

    addDurationVerbiage($format, $seconds, "second");

    $lastCommaIndex = strrpos($format, ',');

    if ( $lastCommaIndex > 0 ) {
        $format = substr($format, 0, $lastCommaIndex) . " and" . substr($format, $lastCommaIndex + 1);
    }

    return $format;
}

echo format_duration(232213);