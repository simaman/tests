<?php
/*
 * Given a group of people with their birth years and death years,
 * write a function which finds the years with the most number of people alive.
 */

function getMax(array $list): array {
    $birthYears = $list[0];
    $deathYears = $list[1];
    $firstBirthYear = min($birthYears);
    $lastDeathYear = max($deathYears);

    $yearsCounted = range($firstBirthYear, $lastDeathYear);

    $rez = [];

    foreach ($yearsCounted as $year) {
        $rez[$year] = 0;

        for ($i = 0;$i < count($list[0]); $i ++) {
            if ( $year >= $list[0][$i]
                && $yearsCounted <= $list[1][$i])
                $rez[$year] ++;
        }
    }

    $maxYears = max($rez);
    $return = [];

    foreach ($rez as $year=>$maxPeople) {
        if ($maxPeople == $maxYears) {
            $return[] = $year;
        }
    }

    return $return;
}
