<?php
/*
 * class that syncs db with input
 */

class Syncer implements ISyncer {
    public Idb $db;
    public string $remoteIdField;
    public array $fieldsToStore;

    public function __construct(Idb $db, string $remoteIdField, array $fieldsToStore) {
        $this->db = $db;
        $this->remoteIdField = $remoteIdField;
        $this->fieldsToStore = array_merge($fieldsToStore, [$remoteIdField]);
    }

    public function sync(array $remoteItems): void {
        $remoteItems = array_column($remoteItems, null, $this->remoteIdField);

        $remoteIds = array_keys($remoteItems);

        $allRecords = $this->db->getAll();
        $dbRemoteIds = array_column($allRecords, $this->remoteIdField);

        $toDeleteRemoteIds = array_diff($dbRemoteIds, $remoteIds);
        $toInsertRemoteIds = array_diff($remoteIds, $dbRemoteIds);

        foreach ($toDeleteRemoteIds as $remoteId) {
            $record = array_filter($allRecords, fn($rec) => $rec[$this->remoteIdField] == $remoteId);
            $this->db->delete(key($record));
        }

        $recordsToSync = $this->db->getBy($this->remoteIdField, $remoteIds);

        foreach ($recordsToSync as $id => &$record) {
            $toSave = false;

            foreach ($this->fieldsToStore as $fieldToStore) {
                $remoteField = $record[$this->remoteIdField];

                if ($record[$fieldToStore]
                    != $remoteItems[$remoteField][$fieldToStore]) {
                    $toSave = true;
                }

                $record[$fieldToStore] = $remoteItems[$remoteField][$fieldToStore];
            }

            if ($toSave) {
                $this->db->update($id, $record);
            }
        }

        foreach ($toInsertRemoteIds as $remoteId) {
            $remoteItem = $remoteItems[$remoteId];
            $record = [];

            foreach ($this->fieldsToStore as $fieldToStore) {
                $record[$fieldToStore] = $remoteItem[$fieldToStore];
            }
            $this->db->insert($record);
        }
    }
}